from django.forms import ModelForm, DateInput, widgets
from lab_1.models import Friend

# Define date input for date of birth
# Source: https://www.youtube.com/watch?v=I2-JYxnSiB0
class InputDOB(DateInput):
    input_type = 'date'

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
        # fields = "__all__" (ALTERNATIVE from GeeksforGeeks)
        widgets = {'dob': InputDOB()}
