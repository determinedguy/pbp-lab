from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from lab_1.models import Friend
from .forms import FriendForm

@login_required(login_url='/admin/login/')
def index(request):
    # Add order_by to show ascending list by name
    friends = Friend.objects.all().order_by('name')
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    # A context is a variable name -> variable value mapping that is passed to a template.
    # Context processors let you specify a number of variables that get set in each context automatically
    # without you having to specify the variables in each render() call.
    # Source: https://stackoverflow.com/questions/20957388/what-is-a-context-in-django
    context = {}
  
    # create object of form
    form = FriendForm(request.POST or None)

    # check if form data is valid (prevent from SQL injection, too)
    if form.is_valid() and request.method == 'POST':
        # save the form data to the model
        form.save()
        # return to index after saving data (POST Redirect)
        return HttpResponseRedirect(reverse('lab_3:index'))
    
    context['form']= form
    return render(request, "lab3_form.html", context)
