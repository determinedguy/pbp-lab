from django.urls import path
from .views import index, add_friend

# Used for POST redirect
# Source: https://stackoverflow.com/a/52902655
app_name = "lab_3"

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add_friend'),
]
