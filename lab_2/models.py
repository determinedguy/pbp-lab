from django.db import models

# Reference: https://docs.djangoproject.com/en/3.2/ref/models/fields/

class Note(models.Model):
    person_to = models.TextField()
    person_from = models.TextField()
    title = models.TextField()
    message = models.TextField()
