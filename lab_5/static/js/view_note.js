$(document).ready(function () {
  $.ajax({
    url: "/lab-2/json",
    success: function (results) {
      results.map((result) => {
        $(".table-body").append(`
          <tr>
            <td class="note-table-data text-muted" id="noteReceiver">${result.fields.person_to}</td>
            <td class="note-table-data text-muted" id="noteSender">${result.fields.person_from}</td>
            <td class="note-table-data text-muted" id="noteTitle"><b>${result.fields.title}</b></td>
            <td class="note-table-data text-muted">
              <button type="button" class="btn btn-primary" id="view-note" value="${result.pk}">View</button>
              <button type="button" class="btn btn-success">Edit</button>
              <button type="button" class="btn btn-danger">Delete</button>
            </td>
          </tr>`);
      });
    },
  });
});
