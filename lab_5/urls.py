from django.urls import path
from .views import index

# Used for POST redirect
# Source: https://stackoverflow.com/a/52902655
app_name = "lab_5"

urlpatterns = [
    path('', index, name='index'),
]
