from django.core import serializers
from django.http.response import HttpResponse
from django.shortcuts import render
from lab_2.models import Note
import json

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    note = Note.objects.get(id = id)
    data = serializers.serialize('json', [note, ])

    struct = json.loads(data)
    data = json.dumps(struct[0])
    return HttpResponse(data, content_type='application/json')