from django.urls import path
from .views import index, add_note, note_list

# Used for POST redirect
# Source: https://stackoverflow.com/a/52902655
app_name = "lab_4"

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add_note'),
    path('note-list', note_list, name='note_list'),
]
