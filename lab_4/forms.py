from typing import Text
from django.forms import ModelForm, Textarea, TextInput
from lab_2.models import Note

# Define widgets
class InputText(TextInput):
    input_type = 'text'

class InputMessage(Textarea):
    input_type = 'text'

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ['person_to', 'person_from', 'title', 'message']
        widgets = {'person_to': InputText(), 'person_from': InputText(), 'title': InputText(), 'message': InputMessage()}
