# Lab 2: Data Delivery Using HTML, XML, and JSON

Nama: Muhammad Athallah<br>
NPM: 2006527481<br>
Kelas: C<br>
Kode Asisten Dosen: WU<br>

## Pertanyaan

1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

## Jawaban

1. JSON bersifat self-describing, sedangkan XML bersifat self-descriptive. Hal ini memengaruhi readability dari JSON dan XML, yang mana JSON lebih mudah dimengerti dibandingkan dengan XML. Selain itu, JSON merupakan turunan dari JavaScript Object sedangkan XML merupakan bahasa markup (seperti HTML). Oleh karena itu, JSON lebih banyak digunakan pada web dinamis (terutama yang diprogram dengan bahasa JavaScript) karena kemudahan interaksi yang ditawarkan oleh JSON sebagai turunan dari objek bahasa pemrograman JavaScript dibandingkan XML yang berbasis bahasa markup. Namun XML memiliki struktur yang kuat dan kokoh dibandingkan JSON, sehingga XML memiliki keuntungan pada web yang menyusun struktur informasi.

2. HTML bersifat statis karena digunakan untuk menampilkan data, sedangkan XML bersifat dinamis karena digunakan untuk membagikan data. Selain itu, HTML tidak bersifat case sensitive sedangkan XML bersifat case sensitive. XML juga membawa data dari dan ke database (sehingga bersifat dinamis), sedangkan HTML hanya dapat menampilkan data (sehingga bersifat statis).

## Referensi

- [What is the difference between HTML vs XML vs JSON?](https://medium.com/@oazzat19/what-is-the-difference-between-html-vs-xml-vs-json-254864972bbb)
- [HTML vs XML - GeeksforGeeks](https://www.geeksforgeeks.org/html-vs-xml/)